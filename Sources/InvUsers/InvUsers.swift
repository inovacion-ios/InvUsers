import Foundation

public struct InvUsers {
    public init() {
    }
    
    public var text = "Hello, World!"
}

public func login(){
    
}

public func register(){
    
}

public func remember(){
    
}

public func changePassword(){
    
}

public func isConfirmed(){
    
}

public func update(){
    
}

public func profile(){
    
}

public func agreesToTermsAndConditions(){
    
}

public func hasPermition(){
    
}

public func hasRole(){
    
}

/**
 This is a method that valdates the format of an email address
 - Parameter email: The string that you want to validate
 - Returns the result of the validation
 */
public func validateEmail(email:String)->Bool{
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email)
}
