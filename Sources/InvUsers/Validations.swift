//
//  Validations.swift
//  Dependencies
//
//  Created by Enxhi Qemalli on 2/28/19.
//

import Foundation

/**
 This is a method that validates the format of a mobile number
 - Parameter number: The string that you want to validate
 - Returns the result of the validation
 */
public func validateMobile(number: String)->Bool{
    if number.count != 16 {
        return false
    }
    let index = number.index(number.startIndex, offsetBy: 7)
    let mySubstring = number[..<index]
    if !(mySubstring == "+355 67" || mySubstring == "+355 68" || mySubstring == "+355 69" ){
        return false
    }
    return true
}

public func isRequired(string: String){
    
}

